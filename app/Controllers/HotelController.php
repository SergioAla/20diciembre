<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

class HotelController 
{
    
    public function HotelLista() {
        
        $hotelModel = new HotelModel();
        $data['title'] = 'Listado de Hoteles';
        $data['hoteles'] = $hotelModel->findAll();     
        return view('hoteles/hotel',$data);    
    }
    
    public function HotelBorrar(){
        
        $id = $this->input->get('id');
        $hotelModel= new HotelModel();
        $hotelModel->delete($id);
        return redirect()->to('HotelController');   
       
}

     public function HotelInsert(){
         
        $hotelController = new HotelController();
        $hotelModel = new HotelModel();
        $data['title']= 'Insertar Hoteles';
        if ($this->request->getMethod() == "post") { 
             $reglas = $hotelModel->getValidationRules();
             $reglas['email'].='|matches[email1]';
             $reglas['email1']='required|valid_email';
             if ($this->validate($reglas)){
                 $solicitud = $this->request->getPost();
                 unset($solicitud['email1']);
                 unset($solicitud['boton']);
                 $hotelModel->insert($solicitud);
                 return redirect()->to('/HotelController');
                 } else {
         
                 
                 $data['errors'] = $this->validator;
             }
        } else { 
        }
        $hoteles=$hotelModel->select('id,nombre')
                ->findAll();
        $data['hoteles'] = changeArray($hoteles, 'id', 'nombre');
        return view('hoteles/insert',$data);
} 


     public function HotelesEditar(){
         
        $hotelController = new HotelController();
        $hotelModel = new HotelModel();
        $data['title'] = 'Editar Hoteles';
        $id = $this->input->get('id'); 
        $resultado = $this->HotelModel->get_hoteles_id($id);
        $nombre = $this->input->post('nombre');
        $data['nombre'] = $nombre;
        if($nombre == NULL) 
        {
            if ($this->form_validation->run() === FALSE) 
            { $this->load->view('hoteles/editar');   
        } else {
            $id = $this->input->post('id');
            $this->HotelModel->update($id, $nombre);
            $this->index();
        }    
     }
     }


}








