<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;

use Codeigniter\Model;


class HotelModel extends Model{
    
   protected $table='hoteles';
   protected $primaryKey='id';
   protected $returnType='object';
}
